package xyz.daos.hibernate.dto;

import java.math.BigDecimal;
import java.util.UUID;

public class TicketDto {
    private UUID id;
    private BigDecimal price;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
