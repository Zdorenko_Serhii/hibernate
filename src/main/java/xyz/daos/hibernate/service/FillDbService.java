package xyz.daos.hibernate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import xyz.daos.hibernate.dao.GenericDao;
import xyz.daos.hibernate.entity.Exposition;
import xyz.daos.hibernate.entity.Hall;
import xyz.daos.hibernate.entity.Status;
import xyz.daos.hibernate.entity.Ticket;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class FillDbService {
    @Autowired
    private GenericDao<Hall> hallDao;

    @Autowired
    private GenericDao<Exposition> expositionDao;

    @Autowired
    private GenericDao<Ticket> ticketDao;


    @Transactional
    public void createHalls() {
        for (int i = 1; i <= 5; i++) {
            Hall hall = new Hall();
            hall.setName("Hall №" + i);
            hall.setArea(10.0 * i);
            hallDao.save(hall);
        }
    }

    @Transactional
    public void createExpositions() {
        for (int i = 1; i <= 2; i++) {
            Exposition exposition = new Exposition();
            exposition.setName("someName" + i);
            exposition.setStatus(Status.ACTIVE);
            exposition.setStartDate(LocalDate.of(2021, 5 + i, 15));
            exposition.setEndDate(LocalDate.of(2021, 5 + i, 20));
            expositionDao.save(exposition);
        }
    }


    @Transactional
    public void expositionsSetHalls() {
        List<Exposition> expositions = expositionDao.getAll();
        List<Hall> halls = hallDao.getAll();

        expositions.get(0).setHalls(new HashSet<>(halls.subList(0, 2)));
        expositions.get(1).setHalls(new HashSet<>(halls.subList(2, 5)));
    }


    @Transactional
    public void createTickets() {
        Set<Ticket> tickets = new HashSet<>();
        for (int i = 1; i <= 4; i++) {
            Ticket ticket = new Ticket();
            ticket.setPrice(BigDecimal.valueOf(10));
            tickets.add(ticket);
            ticketDao.save(ticket);
        }
    }

    @Transactional
    public void expositionsSetTickets() {
        List<Exposition> expositions = expositionDao.getAll();
        List<Ticket> tickets = ticketDao.getAll();

        expositions.get(0).getTickets().addAll(tickets.subList(0, 2));
        expositions.get(1).getTickets().addAll(tickets.subList(2, 4));
    }

}
