package xyz.daos.hibernate.service;

import xyz.daos.hibernate.dto.ExpositionDto;

import java.util.List;
import java.util.UUID;

public interface ExpositionService {
    ExpositionDto create(ExpositionDto expositionDto);

    ExpositionDto get(UUID id);

    ExpositionDto update(ExpositionDto expositionDto);

    void delete(UUID id);

    List<ExpositionDto> getAllExpositions();

    ExpositionDto getByName(String name);
}
