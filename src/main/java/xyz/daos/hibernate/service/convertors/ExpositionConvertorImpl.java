package xyz.daos.hibernate.service.convertors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.daos.hibernate.dao.GenericDao;
import xyz.daos.hibernate.dto.ExpositionDto;
import xyz.daos.hibernate.entity.Exposition;

import java.util.stream.Collectors;

@Component
public class ExpositionConvertorImpl implements ExpositionConvertor {

    @Autowired
    private GenericDao<Exposition> expositionDao;

    @Autowired
    private TicketConvertor ticketConvertor;

    @Autowired
    private HallConvertor hallConvertor;

    @Override
    public Exposition convert(ExpositionDto expositionDto) {
        Exposition exposition = new Exposition();
        exposition.setId(expositionDto.getId());
        exposition.setName(expositionDto.getName());
        exposition.setStartDate(expositionDto.getStartDate());
        exposition.setEndDate(expositionDto.getEndDate());
        exposition.setStatus(expositionDto.getStatus());

        if (!expositionDto.getTickets().isEmpty()) {
            exposition.setTickets(expositionDto.getTickets()
                    .stream()
                    .map(ticketDto -> ticketConvertor.convert(ticketDto))
                    .collect(Collectors.toSet())
            );
        }

        if (!expositionDto.getHalls().isEmpty()) {
            exposition.setHalls(expositionDto.getHalls()
                    .stream()
                    .map(hallDto -> hallConvertor.convert(hallDto))
                    .collect(Collectors.toSet())
            );
        }

        return exposition;
    }

    @Override
    public ExpositionDto convert(Exposition exposition) {
        ExpositionDto expositionDto = new ExpositionDto();
        expositionDto.setId(exposition.getId());
        expositionDto.setName(exposition.getName());
        expositionDto.setStartDate(exposition.getStartDate());
        expositionDto.setEndDate(exposition.getEndDate());
        expositionDto.setStatus(exposition.getStatus());

        if (!exposition.getTickets().isEmpty()) {
            expositionDto.setTickets(exposition.getTickets()
                    .stream()
                    .map(ticketDto -> ticketConvertor.convert(ticketDto))
                    .collect(Collectors.toSet())
            );
        }

        if (!exposition.getHalls().isEmpty()) {
            expositionDto.setHalls(exposition.getHalls()
                    .stream()
                    .map(hallDto -> hallConvertor.convert(hallDto))
                    .collect(Collectors.toSet())
            );
        }

        return expositionDto;
    }
}
