package xyz.daos.hibernate.service.convertors;

import xyz.daos.hibernate.dto.TicketDto;
import xyz.daos.hibernate.entity.Ticket;

public interface TicketConvertor {
    Ticket convert(TicketDto ticketDto);

    TicketDto convert(Ticket ticket);
}
