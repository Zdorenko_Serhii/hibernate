package xyz.daos.hibernate.service.convertors;

import xyz.daos.hibernate.dto.HallDto;
import xyz.daos.hibernate.entity.Hall;

public interface HallConvertor {
    Hall convert(HallDto hallDto);

    HallDto convert(Hall hall);
}
