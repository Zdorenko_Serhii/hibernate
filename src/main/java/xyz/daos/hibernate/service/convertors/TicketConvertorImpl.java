package xyz.daos.hibernate.service.convertors;

import org.springframework.stereotype.Component;
import xyz.daos.hibernate.dto.TicketDto;
import xyz.daos.hibernate.entity.Ticket;

@Component
public class TicketConvertorImpl implements TicketConvertor {
    @Override
    public Ticket convert(TicketDto ticketDto) {
        Ticket ticket = new Ticket();
        ticket.setId(ticketDto.getId());
        ticket.setPrice(ticketDto.getPrice());
        return ticket;
    }

    @Override
    public TicketDto convert(Ticket ticket) {
        TicketDto ticketDto = new TicketDto();
        ticketDto.setId(ticket.getId());
        ticketDto.setPrice(ticket.getPrice());
        return ticketDto;
    }
}
