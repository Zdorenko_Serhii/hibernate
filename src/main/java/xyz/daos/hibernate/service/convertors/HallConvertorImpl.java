package xyz.daos.hibernate.service.convertors;

import org.springframework.stereotype.Component;
import xyz.daos.hibernate.dto.HallDto;
import xyz.daos.hibernate.entity.Hall;

@Component
public class HallConvertorImpl implements HallConvertor {
    @Override
    public Hall convert(HallDto hallDto) {
        Hall hall = new Hall();
        hall.setId(hallDto.getId());
        hall.setName(hallDto.getName());
        hall.setArea(hallDto.getArea());
        return hall;
    }

    @Override
    public HallDto convert(Hall hall) {
        HallDto hallDto = new HallDto();
        hallDto.setId(hall.getId());
        hallDto.setName(hall.getName());
        hallDto.setArea(hall.getArea());
        return hallDto;
    }
}
