package xyz.daos.hibernate.service.convertors;

import xyz.daos.hibernate.dto.ExpositionDto;
import xyz.daos.hibernate.entity.Exposition;

public interface ExpositionConvertor {
    Exposition convert(ExpositionDto expositionDto);

    ExpositionDto convert(Exposition exposition);
}
