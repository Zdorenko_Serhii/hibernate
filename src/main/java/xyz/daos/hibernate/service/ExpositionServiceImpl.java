package xyz.daos.hibernate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.daos.hibernate.dao.GenericDao;
import xyz.daos.hibernate.dto.ExpositionDto;
import xyz.daos.hibernate.entity.Exposition;
import xyz.daos.hibernate.entity.Hall;
import xyz.daos.hibernate.entity.Ticket;
import xyz.daos.hibernate.service.convertors.ExpositionConvertor;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ExpositionServiceImpl implements ExpositionService {
    private GenericDao<Exposition> dao;
    private ExpositionConvertor convertor;

    @Autowired
    public ExpositionServiceImpl(GenericDao<Exposition> dao, ExpositionConvertor convertor) {
        this.dao = dao;
        this.convertor = convertor;
    }

    @Override
    public ExpositionDto create(ExpositionDto dto) {
        Exposition exposition = convertor.convert(dto);
        dao.save(exposition);
        return convertor.convert(exposition);
    }

    @Override
    @Transactional(readOnly = true)
    public ExpositionDto get(UUID id) {
        Exposition exposition = dao.get(id);
        return convertor.convert(exposition);
    }

    @Override
    @Cacheable(value = "expoCache", key = "#name")
    @Transactional(readOnly = true)
    public ExpositionDto getByName(String name) {
        // to test caching
        try {
            TimeUnit.MILLISECONDS.sleep(1000);
        } catch (InterruptedException e) {}

        Exposition exposition = dao.getByName(name);
        return convertor.convert(exposition);
    }

    @Override
    @Transactional
    public ExpositionDto update(ExpositionDto expositionDto) {
        Exposition entity = dao.get(expositionDto.getId());
        Exposition updatedEntity = convertor.convert(expositionDto);

        performUpdate(entity, updatedEntity);

        return convertor.convert(entity);
    }

    private void performUpdate(Exposition persistentEntity, Exposition updatedEntity) {
        persistentEntity.setName(updatedEntity.getName());
        persistentEntity.setStartDate(updatedEntity.getStartDate());
        persistentEntity.setEndDate(updatedEntity.getEndDate());
        persistentEntity.setStatus(updatedEntity.getStatus());

        updateHalls(persistentEntity.getHalls(), updatedEntity.getHalls());
        updateTickets(persistentEntity.getTickets(), updatedEntity.getTickets());
    }

    private void updateHalls(Set<Hall> persistentHalls, Set<Hall> updatedHalls) {
        Map<UUID, Hall> stillExistentHalls = updatedHalls
                .stream()
                .filter(hall -> Objects.nonNull(hall.getId()))
                .collect(Collectors.toMap(Hall::getId, Function.identity()));

        List<Hall> hallsToAdd = updatedHalls
                .stream()
                .filter(hall -> Objects.isNull(hall.getId()))
                .collect(Collectors.toList());

        Iterator<Hall> persistentIterator = persistentHalls.iterator();
        while (persistentIterator.hasNext()) {
            Hall persistentHall = persistentIterator.next();
            if (stillExistentHalls.containsKey(persistentHall.getId())) {
                Hall updatedHall = stillExistentHalls.get(persistentHall.getId());
                persistentHall.setName(updatedHall.getName());
                persistentHall.setArea(updatedHall.getArea());
            } else {
                persistentHalls.remove(persistentHall);
            }
        }

        persistentHalls.addAll(hallsToAdd);
    }

    private void updateTickets(Set<Ticket> persistentTickets, Set<Ticket> updatedTickets) {
        Map<UUID, Ticket> stillExistentTickets = updatedTickets
                .stream()
                .filter(ticket -> Objects.nonNull(ticket.getId()))
                .collect(Collectors.toMap(Ticket::getId, Function.identity()));

        List<Ticket> ticketsToAdd = updatedTickets
                .stream()
                .filter(ticket -> Objects.isNull(ticket.getId()))
                .collect(Collectors.toList());

        Iterator<Ticket> persistentIterator = persistentTickets.iterator();
        while (persistentIterator.hasNext()) {
            Ticket persistentTicket = persistentIterator.next();
            if (stillExistentTickets.containsKey(persistentTicket.getId())) {
                Ticket updatedTicket = stillExistentTickets.get(persistentTicket.getId());
                persistentTicket.setPrice(updatedTicket.getPrice());
            } else {
                persistentTickets.remove(persistentTicket);
            }
        }
        persistentTickets.addAll(ticketsToAdd);
    }


    @Override
    @Transactional
    public void delete(UUID id) {
        dao.delete(id);
    }


    @Transactional(readOnly = true)
    public List<ExpositionDto> getAllExpositions() {
        List<Exposition> expositions = dao.getAll();
        return expositions.stream().map(exposition -> convertor.convert(exposition)).collect(Collectors.toList());
    }
}
