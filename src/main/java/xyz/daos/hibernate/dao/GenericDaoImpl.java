package xyz.daos.hibernate.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.UUID;

public abstract class GenericDaoImpl<T> implements GenericDao<T> {
    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> type;

    protected GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class<T>) pt.getActualTypeArguments()[0];
    }


    @Override
    @Transactional
    public void save(T entity) {
        sessionFactory.getCurrentSession().save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public T get(UUID id) {
        return sessionFactory.getCurrentSession().get(type, id);
    }

    @Override
    @Transactional
    public void update(T entity) {
        sessionFactory.getCurrentSession().update(entity);
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        sessionFactory.getCurrentSession().delete(get(id));
    }
}
