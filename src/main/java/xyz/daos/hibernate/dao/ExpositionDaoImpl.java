package xyz.daos.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import xyz.daos.hibernate.entity.Exposition;

import java.util.List;

@Repository
public class ExpositionDaoImpl extends GenericDaoImpl<Exposition> {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(readOnly = true)
    public List<Exposition> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Exposition").list();
    }

    @Transactional(readOnly = true)
    public Exposition getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Object result = session
                .getNamedQuery("expoByName")
                .setParameter("name", name)
                .getSingleResult();
        return (Exposition) result;
    }

}
