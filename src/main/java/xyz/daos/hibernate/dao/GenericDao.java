package xyz.daos.hibernate.dao;

import java.util.List;
import java.util.UUID;

public interface GenericDao<T> {
    void save(T entity);

    T get(UUID id);

    void update(T entity);

    void delete(UUID id);

    List<T> getAll();

    T getByName(String name);
}
