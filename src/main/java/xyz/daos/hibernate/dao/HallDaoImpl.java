package xyz.daos.hibernate.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import xyz.daos.hibernate.entity.Hall;

import java.util.List;

@Repository
public class HallDaoImpl extends GenericDaoImpl<Hall> {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Hall> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Hall").list();
    }

    @Override
    @Deprecated
    public Hall getByName(String name) {
        return null;
    }
}
