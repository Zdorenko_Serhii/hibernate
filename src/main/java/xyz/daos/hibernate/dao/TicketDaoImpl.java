package xyz.daos.hibernate.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import xyz.daos.hibernate.entity.Ticket;

import java.util.List;

@Repository
public class TicketDaoImpl extends GenericDaoImpl<Ticket> {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Ticket> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Ticket").list();
    }

    @Override
    @Deprecated
    public Ticket getByName(String name) {
        return null;
    }
}
