package xyz.daos.hibernate.entity;

public enum Status {
    CREATED, ACTIVE, CANCELLED, ENDED;
}
