package xyz.daos.hibernate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import xyz.daos.hibernate.dto.ExpositionDto;
import xyz.daos.hibernate.service.ExpositionService;
import xyz.daos.hibernate.service.FillDbService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/expositions")
public class ExpositionController {

    @Autowired
    private ExpositionService expositionService;

    @Autowired
    FillDbService fillDbService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody ExpositionDto expositionDto) {
        expositionService.create(expositionDto);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ExpositionDto read(@PathVariable String id) {
        return expositionService.get(UUID.fromString(id));
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ExpositionDto update(@RequestBody ExpositionDto expositionDto) {
        return expositionService.update(expositionDto);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable UUID id) {
        expositionService.delete(id);
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ExpositionDto> getAllExpositions() {
        return expositionService.getAllExpositions();
    }

    @GetMapping(value = "/create")
    @ResponseStatus(HttpStatus.OK)
    public void createEntities() {
        fillDbService.createHalls();
        fillDbService.createExpositions();
        fillDbService.expositionsSetHalls();
        fillDbService.createTickets();
        fillDbService.expositionsSetTickets();
    }
}
