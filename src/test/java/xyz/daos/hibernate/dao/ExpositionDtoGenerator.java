package xyz.daos.hibernate.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.daos.hibernate.dto.ExpositionDto;
import xyz.daos.hibernate.dto.HallDto;
import xyz.daos.hibernate.dto.TicketDto;
import xyz.daos.hibernate.entity.Hall;
import xyz.daos.hibernate.entity.Status;
import xyz.daos.hibernate.entity.Ticket;
import xyz.daos.hibernate.service.convertors.HallConvertor;
import xyz.daos.hibernate.service.convertors.TicketConvertor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Component
public class ExpositionDtoGenerator {
    @Autowired
    GenericDao<Hall> hallDao;

    @Autowired
    GenericDao<Ticket> ticketDao;

    @Autowired
    HallConvertor hallConvertor;

    @Autowired
    TicketConvertor ticketConvertor;


    ExpositionDto createExpositionDto() {
        ExpositionDto expositionDto = new ExpositionDto();
        expositionDto.setName("TEST EXPO");
        expositionDto.setStatus(Status.ACTIVE);
        expositionDto.setStartDate(LocalDate.of(2021, 5, 15));
        expositionDto.setEndDate(LocalDate.of(2021, 5, 20));

        HallDto hallDto = new HallDto();
        hallDto.setArea(50.0);
        hallDto.setName("TEST HALL");
        hallDao.save(hallConvertor.convert(hallDto));

        expositionDto.getHalls().add(hallDto);

        TicketDto ticketDto = new TicketDto();
        ticketDto.setPrice(BigDecimal.valueOf(500));
        ticketDao.save(ticketConvertor.convert(ticketDto));

        expositionDto.getTickets().add(ticketDto);

        return expositionDto;
    }
}
