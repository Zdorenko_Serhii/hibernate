package xyz.daos.hibernate.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import xyz.daos.hibernate.config.HibernateConfig;
import xyz.daos.hibernate.dto.ExpositionDto;
import xyz.daos.hibernate.service.ExpositionService;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringJUnitConfig(HibernateConfig.class)
@WebAppConfiguration
public class CachingTest {

    @Autowired
    ExpositionService expositionService;

    @Autowired
    ExpositionDtoGenerator generator;

    @Test
    @Transactional
    void testGetExpositionByName() {
        ExpositionDto expositionDto = generator.createExpositionDto();
        expositionService.create(expositionDto);
        long totalTime = 0;
        for (int i = 0; i < 10; i++) {
            long startTime = System.currentTimeMillis();
            expositionService.getByName(expositionDto.getName());
            totalTime += System.currentTimeMillis() - startTime;
        }
        assertTrue( (totalTime / 10 < 500));
    }
}
