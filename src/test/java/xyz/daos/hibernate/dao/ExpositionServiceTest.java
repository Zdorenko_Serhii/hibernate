package xyz.daos.hibernate.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import xyz.daos.hibernate.config.HibernateConfig;
import xyz.daos.hibernate.dto.ExpositionDto;
import xyz.daos.hibernate.dto.HallDto;
import xyz.daos.hibernate.dto.TicketDto;
import xyz.daos.hibernate.entity.Status;
import xyz.daos.hibernate.service.ExpositionService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringJUnitConfig(HibernateConfig.class)
@WebAppConfiguration
class ExpositionServiceTest {

    @Autowired
    ExpositionService expositionService;

    @Autowired
    ExpositionDtoGenerator generator;

    @Test
    @Transactional
    void testCreateExposition() {
        ExpositionDto expositionDto = generator.createExpositionDto();
        ExpositionDto createdExposition = expositionService.create(expositionDto);

        assertEquals(expositionDto.getName(), createdExposition.getName());
        assertEquals(expositionDto.getHalls().size(), createdExposition.getHalls().size());
        assertEquals(expositionDto.getTickets().size(), createdExposition.getTickets().size());
    }

    @Test
    @Transactional
    void testGetExpositionById() {
        ExpositionDto expositionDto = generator.createExpositionDto();
        UUID id = expositionService.create(expositionDto).getId();
        ExpositionDto createdExposition = expositionService.get(id);

        assertEquals(expositionDto.getName(), createdExposition.getName());
        assertEquals(expositionDto.getHalls().size(), createdExposition.getHalls().size());
        assertEquals(expositionDto.getTickets().size(), createdExposition.getTickets().size());
    }

    @Test
    @Transactional
    void testGetExpositionByName() {
        ExpositionDto expositionDto = generator.createExpositionDto();
        expositionService.create(expositionDto);
        ExpositionDto createdExposition = expositionService.getByName(expositionDto.getName());

        assertEquals(expositionDto.getName(), createdExposition.getName());
        assertEquals(expositionDto.getHalls().size(), createdExposition.getHalls().size());
        assertEquals(expositionDto.getTickets().size(), createdExposition.getTickets().size());
    }

    @Test
    @Transactional
    void testDelete() {
        ExpositionDto expositionDto = generator.createExpositionDto();
        ExpositionDto createdExposition = expositionService.create(expositionDto);
        expositionService.delete(createdExposition.getId());
    }

    @Test
    @Transactional
    void testUpdate() {
        ExpositionDto expositionDto = generator.createExpositionDto();
        ExpositionDto createdExposition = expositionService.create(expositionDto);

        createdExposition.setName("NEW NAME");
        createdExposition.setStatus(Status.ENDED);
        createdExposition.setStartDate(LocalDate.of(2022, 1, 1));
        createdExposition.setEndDate(LocalDate.of(2023, 1, 1));

        createdExposition.getTickets().clear();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setPrice(BigDecimal.valueOf(1000));
        createdExposition.getTickets().add(ticketDto);

        HallDto hallDto = new HallDto();
        hallDto.setArea(100.0);
        hallDto.setName("NEW TEST HALL");
        createdExposition.getHalls().add(hallDto);

        expositionService.update(createdExposition);

        ExpositionDto updatedExposition = expositionService.get(createdExposition.getId());


        assertEquals("NEW NAME", updatedExposition.getName());
        assertEquals(Status.ENDED, updatedExposition.getStatus());
        assertEquals(LocalDate.of(2022, 1, 1), updatedExposition.getStartDate());
        assertEquals(LocalDate.of(2023, 1, 1), updatedExposition.getEndDate());

        assertEquals(2, updatedExposition.getHalls().size());

        assertEquals(1, updatedExposition.getTickets().size());
    }
}


















